arrays: main.o strings.o unity.o
	gcc main.o strings.o unity.o -o strings

main.o: main.c strings.h unity.h unity_internals.h
	gcc -c main.c

strings.o: strings.c strings.h
	gcc -c strings.c

unity.o: unity.c unity.h unity_internals.h
	gcc -c unity.c

clean:
	rm -f *~ strings *.o
