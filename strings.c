#include <ctype.h>
#include "strings.h"

int palindrome(char *str) {
  int i, length;
  char buffer[256];

  for (i = 0; str[i]; i++) {
    buffer[i] = tolower(str[i]);
  }
  buffer[i] = 0;
  length = i;
  for (i = 0; i < length / 2; i++) {
    if (buffer[i] != buffer[length - 1 - i]) {
      return 0;
    }
  }
  return 1;
}
