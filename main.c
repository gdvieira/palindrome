#include "unity.h"
#include "strings.h"

void setUp() {
}

void tearDown() {
}

void test_palindrome() {
  TEST_ASSERT_TRUE(palindrome("anna"));
}

void test_nao_palindrome() {
  TEST_ASSERT_FALSE(palindrome("pedro"));
}

void test_palindrome_impar() {
  TEST_ASSERT_TRUE(palindrome("ana"));
}

void test_palindrome_unico() {
  TEST_ASSERT_TRUE(palindrome("a"));
}

void test_palindrome_vazio() {
  TEST_ASSERT_TRUE(palindrome(""));
}

void test_palindrome_misturado() {
  TEST_ASSERT_TRUE(palindrome("Ana"));
}

void test_palindrome_misturado_impar() {
  TEST_ASSERT_TRUE(palindrome("aNa"));
}

int main() {
  UNITY_BEGIN();
  RUN_TEST(test_palindrome);
  RUN_TEST(test_nao_palindrome);
  RUN_TEST(test_palindrome_impar);
  RUN_TEST(test_palindrome_unico);
  RUN_TEST(test_palindrome_vazio);
  RUN_TEST(test_palindrome_misturado);
  RUN_TEST(test_palindrome_misturado_impar);
  return UNITY_END();
}
